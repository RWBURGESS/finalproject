""" 
Script to reduce original data for 573 project to less than 1GB.

The data analyzed were originally contained in 2 files of approximately 4GB and can be found here:
https://goldsmr5.gesdisc.eosdis.nasa.gov/data/MERRA2/M2I3NVAER.5.12.4/2020/06/ for the first 
dataset and here: https://goldsmr5.gesdisc.eosdis.nasa.gov/data/MERRA2/M2I6NVANA.5.12.4/2020/06/
for the second. The first file contains the dust mixing ratio (DUMR) for 5 different aerosol sizes, 
as well as DELP, which is the change in pressure at each level. The second file contains temperature, 
zonal and meridional wind, specific humidity and DELP. Each of these variables have 4 dimensions (time, 
level, latitude and longitude). The data were averaged over time for an entire day and the 5 variables 
for different sizes of DUMR were added together to create a variable for total DUMR. Other variables 
that were not used in the analysis were dropped in both datasets. The final datasets were saved to two
netcdf files.

"""

import xarray as xr

# Read in files
dataFile = '/Users/rubyburgess/Desktop/merra_data/202006/MERRA2_400.inst3_3d_aer_Nv.20200625.nc4'
dataFile2 = '/Users/rubyburgess/Desktop/test_download/MERRA2_400.inst6_3d_ana_Nv.20200625.nc4'

ds = xr.open_dataset(dataFile) # first dataset (DUMR,DELP)
ds2 = xr.open_dataset(dataFile2) # second dataset (T,U,V,QV,DELP)

ds['DUMR'] = (ds['DU001'] + ds['DU002'] + ds['DU003'] + ds['DU004'] + ds['DU005']).mean('time') #add DUMR for 5 different aerosol sizes and take average over time
ds['DELP'] = ds['DELP'].mean('time') # average DELP over time
ds=ds.drop(['AIRDENS','BCPHILIC','BCPHOBIC','DMS','DU001','DU002','DU003','DU004','DU005','LWI','MSA','OCPHOBIC','OCPHILIC','PS','RH','SO2','SO4','SS001','SS002','SS003','SS004','SS005']) #drop data variables that we don't need

ds2 = ds2.drop(['PS','O3']) #drop data variables we don't need in second dataset
ds2 = ds2.mean('time') #average dataset over time

# Export datasets
ds.to_netcdf('DUMR_data.nc4')
ds2.to_netcdf('RH_T_data.nc4')